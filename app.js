var http = require("http"),
    url = require("url"), // Parse, interpret and manipulate URLs
    fs = require("fs"), // Handle files
    io = require("socket.io"),
    five = require("johnny-five"); // Instatiating socket.io

var board = new five.Board({ port: "COM3" }), // (Optional) Strictly specifying the port to connect, since the PC I'm using keep recognizing other devices as the board
    servos = null,
    servoPins = [9, 10];

var horizontalAngle = 90.0, verticalAngle = 90.0;

var server = http.createServer(function(request, response){
    var path = url.parse(request.url).pathname;

    // Basic routing
    if(path === "/"){
        response.writeHead(200, {"Content-Type": "text/html"});
        response.write("Hello World. If you're on a phone please go to /phone.html");
        response.end();
    } else if(path === "/phone.html" || path === "styles/style.css"){
        fs.readFile(__dirname + path, function(error, data){
            if(error){
                response.writeHead(404);
                response.write("I'm afraid I cannot let you do that. - 404");
                response.end();
            } else {
                response.writeHead(200, {"Content-Type": "text/html"});
                response.write(data, "utf8");
                response.end();
            }
        });
    } else {
        response.writeHead(404);
        response.write("There's nothing here. - 404");
        response.end();
    }
});

server.listen(8788);
console.log("Server now listening on port 8788");

board.on("ready", function(){
    console.log("Arduino board ready, initializing servo array.");
    servos = new five.Servos(servoPins);

    console.log("Centering servos in array.");
    servos.to(90.0);
});

var listener = io.listen(server);
listener.sockets.on('connection', function(socket){
    socket.on('deviceMove', function(data){
        horizontalAngle += data.rotateGamma;
        verticalAngle += data.rotateAlpha;

        if(horizontalAngle > 165)
            horizontalAngle = 165;

        if(horizontalAngle < 0)
            horizontalAngle = 0;

        if(verticalAngle > 165)
            verticalAngle = 165;

        if(verticalAngle < 50)
            verticalAngle = 50;

        console.log("Horizontal Angle: " + horizontalAngle + ", Vertical Angle: " + verticalAngle);

        servos[0].to(horizontalAngle);
        servos[1].to(verticalAngle);
    });
});